__BANNER__ = '''==========================================================================================================================

PREDI-XY Version {version}

==========================================================================================================================

A program for automated generation of NICS-XY-Scans for 
cata-condensed polycyclic aromatic systems using additivty

Developed by:       Alexandra Wahab, Felix Fleckenstein, Stefan Feusi, and Renana Gershoni-Poranne

If you use this program, please cite:
    A. Wahab, F. Fleckenstein, S. Feusi and R. Gershoni-Poranne, Electronic Structure 2020, DOI: 10.1088/2516-1075/abd081
    P. Finkelstein and R. Gershoni-Poranne, ChemPhysChem 2019, 20, 1508-1520
    R. Gershoni-Poranne, Chemistry - A European Journal 2018, 24, 4165-4172
    R. Gershoni-Poranne and A. Stanger, Chemistry - A European Journal 2014, 20, 6573-5688

==========================================================================================================================
'''

__SEPARATOR__ = "=========================================================================================================================="