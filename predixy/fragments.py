__FRAGMENTS__ = {
    "bn":("benzene", "CCCCCC"),
    "pyd":("pyridine", "NCCCCC"),
    "pyl":("pyrrole", "NCCCC"),
    "pyz":("1,4-pyrazine", "NCCNCC"),
    "fur":("furan", "OCCCC"),
    "thi":("thiophene", "SCCCC"),
    "dbrn":("1,4-diboronine", "BCCBCC"),
    "dhbn":("1,4-dihydro-1,4-diboronine", "BHCCBHCC")
}